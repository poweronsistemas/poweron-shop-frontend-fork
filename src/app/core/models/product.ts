export class Product {
  id: string;
  brand: string;
  model: string;
  color: string;
  description: string;
  tags: string[];
  weight: number;
  salePrice: number;
  saleCurrency: string;
  costPrice: number;
  costCurrency: number;
  image: string;
  createdDate: Date;
  stock: number;
}
